/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.json.JSONArray;
import org.modmacao.occi.platform.Component;

import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.connector.MartConnector;
import de.ugoe.cs.rwm.docci.executor.MartExecutor;
import de.ugoe.cs.rwm.tocci.occi2openstack.OCCI2OPENSTACKTransformator;

/**
 * Making javadoc happy.
 *
 * @author erbel
 *
 */
public class MAPE {
	protected static final Path RUNTIMEPATH = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
	static Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
	// static Connector conn = new MartConnector("192.168.35.45", 8080, "ubuntu",
	// "~/key.pem");
	static MartExecutor executor = new MartExecutor(conn);
	static Resource runtimeModel;
	static Component newComp;
	static int interval = 5000;
	static boolean manualMode = false;

	/**
	 * Making javadoc happy.
	 *
	 * @param args Making javadoc happy.
	 */
	public static void main(String[] args) {
		System.out.println("Starting MAPE loop");
		RegistryAndLoggerSetup.setup();

		for (String arg : args) {
			if (arg.matches("\\d+")) {
				System.out.println("Cycle argument set to: " + arg);
				interval = Integer.parseInt(arg);
			}
			if (arg.matches("manual")) {
				System.out.println("Manual mode enabled!");
				manualMode = true;
			}
		}

		if (args.length == 0) {
			System.out.println("No argument given. Using default cycle of: " + interval);
		}

		while (true) {
			try {
				if (manualMode) {
					System.out.println("Press Enter to trigger next cycle or write exit to stop the loop.");
					Scanner scanner = new Scanner(System.in);
					String line = scanner.nextLine();
					if (line.equals("exit")) {
						return;
					}
					System.out.println("Next Cycle triggered");
				} else {
					System.out.println("\n--------------------Waiting for new MAPE-K Cycle: Sleeping " + interval
							+ "--------------------");
					Thread.sleep(interval);
				}

				Monitor monitor = monitor();
				String analysis = analyze(monitor);
				runtimeModel = plan(analysis);
				execute(runtimeModel);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static Monitor monitor() {
		int critCPUs = getNumberOfCriticalCPUs();
		int noneCPUs = getNumberOfNoneCPUs();
		int allCPUs = getNumberOfAllCPUs();
		Monitor mon = new Monitor(critCPUs, noneCPUs, allCPUs);
		System.out.println("Monitor: " + "Monitored CPUs: " + allCPUs + "| Critical CPUs: " + critCPUs + "| None CPUs: "
				+ noneCPUs);
		return mon;
	}

	public static String analyze(Monitor monitor) {
		int noneCPUs = monitor.getNoneCPUs();
		int critCPUs = monitor.getCritCPUs();
		int allCPUs = monitor.getAllCPUs();

		if (noneCPUs == 0 && critCPUs > allCPUs / 2 && allCPUs <= 6) {
			System.out.println("Analyze: Critical State Detected");
			return "upScale";
		} else if (allCPUs > 2) {
			System.out.println("Analyze: Non Critical State Detected");
			return "downScale";
		}
		return "noScale";
	}

	public static Resource plan(String analysis) {
		switch (analysis) {
		case "upScale":
			System.out.println("Plan: upScale!");
			UpScalerSpark upscaler = new UpScalerSpark(conn, RUNTIMEPATH);
			return upscaler.upScaleNodes();
		case "downScale":
			System.out.println("Plan: downScale!");
			DownScaler downscaler = new DownScaler(conn, RUNTIMEPATH);
			return downscaler.downScaleNodes();
		}
		return ModelUtility.loadOCCIintoEMFResource(RUNTIMEPATH);

	}

	public static void execute(Resource runtimeModel) {
		if (runtimeModel == null) {
			System.out.println("Execute: Skipped as no scaling planned");
		}
		System.out.println("Execute: Deploying adjusted Model");

		Path occiPath = RUNTIMEPATH;
		OCCI2OPENSTACKTransformator trans2 = new OCCI2OPENSTACKTransformator();
		trans2.setTransformationProperties(RegistryAndLoggerSetup.manNWRuntimeId, RegistryAndLoggerSetup.sshKey,
				RegistryAndLoggerSetup.userData, RegistryAndLoggerSetup.manNWid);

		try {
			trans2.transform(runtimeModel, occiPath);
		} catch (EolRuntimeException e) {
			e.printStackTrace();
		}
		runtimeModel = ModelUtility.loadOCCIintoEMFResource(occiPath);

		MartDeployer deployer;
		if (conn instanceof MartConnector) {
			deployer = new MartDeployer(conn, 1000);
		} else {
			deployer = new MartDeployer(conn);
		}
		deployer.deploy(runtimeModel);

		if (newComp != null) {
			executor.executeOperation("POST", newComp, ModelUtility.getAction(newComp, "start"));
		}
	}

	private static int getNumberOfNoneCPUs() {
		String query = "/monitorableproperty?attribute=monitoring.result&value=None";
		String result = executor.executeGetOperation(query);
		if (result.equals("{ }") == false) {
			String substring = result.substring(result.indexOf("["), (result.lastIndexOf("]") + 1));
			JSONArray arr = new JSONArray(substring);
			if (arr.length() == 0) {
				return 1;
			} else {
				return arr.length();
			}
		} else {
			return 0;
		}

	}

	private static int getNumberOfCriticalCPUs() {
		String query = "/monitorableproperty?attribute=monitoring.result&value=Critical";
		String result = executor.executeGetOperation(query);
		if (result.equals("{ }") == false) {
			String substring = result.substring(result.indexOf("["), (result.lastIndexOf("]") + 1));
			JSONArray arr = new JSONArray(substring);
			if (arr.length() == 0) {
				return 1;
			} else {
				return arr.length();
			}
		} else {
			return 0;
		}
	}

	private static int getNumberOfAllCPUs() {
		String query = "/monitorableproperty?attribute=monitoring.property&value=CPU";
		String result = executor.executeGetOperation(query);
		if (result.equals("{ }") == false) {
			String substring = result.substring(result.indexOf("["), (result.lastIndexOf("]") + 1));
			JSONArray arr = new JSONArray(substring);
			if (arr.length() == 0) {
				return 1;
			} else {
				return arr.length();
			}
		} else {
			return 0;
		}
	}

}