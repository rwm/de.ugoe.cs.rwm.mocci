/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.domain.workload.Componentsim;
import de.ugoe.cs.rwm.domain.workload.Computesim;
import de.ugoe.cs.rwm.domain.workload.Sensorsim;
import de.ugoe.cs.rwm.domain.workload.WorkloadFactory;
import monitoring.*;
import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.docker.Container;
import org.eclipse.cmf.occi.docker.DockerFactory;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.ComputeStatus;
import org.eclipse.emf.ecore.resource.Resource;
import org.modmacao.occi.platform.Application;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.Componentlink;
import org.modmacao.placement.Placementlink;

import java.nio.file.Path;

public class UpScalerSim extends AbsScaler {

	public UpScalerSim(Connector conn, Path runtimePath) {
		this.conn = conn;
		this.runtimePath = runtimePath;
	}

	@SuppressWarnings("unused")
	public Resource upScaleNodes() {
		runtimeModel = ModelUtility.loadOCCIintoEMFResource(conn.loadRuntimeModel(runtimePath));
		Configuration config = ((Configuration) runtimeModel.getContents().get(0));

		Compute comp = addCompute(config);
		Component worker = addWorkerComponent(config, comp);
		Sensor sens = addSensor(config, comp);
		Resultprovider rp = addResultProvider(config, comp, sens);

		config.getResources().add(comp);
		config.getResources().add(worker);
		config.getResources().add(sens);
		config.getResources().add(rp);

		return runtimeModel;
	}

	private Compute addCompute(Configuration config) {
		System.out.println("         Adding Compute Node to Model");
		Compute comp = iFactory.createCompute();
		comp.setOcciComputeState(ComputeStatus.ACTIVE);
		Container cont = DockerFactory.eINSTANCE.createContainer();
		comp.setKind(cont.getKind());

		AttributeState state = factory.createAttributeState();
		state.setName("occi.compute.state");
		state.setValue("active");
		comp.getAttributes().add(state);
		config.getResources().add(comp);

		comp.setTitle("Hadoop-worker-additional");

		AttributeState hostname = factory.createAttributeState();
		hostname.setName("occi.compute.hostname");
		hostname.setValue("Hadoop-worker-additional");
		comp.getAttributes().add(hostname);

		Computesim sim = WorkloadFactory.eINSTANCE.createComputesim();
		comp.getParts().add(sim);

		return comp;
	}

	private Component addWorkerComponent(Configuration config, Compute comp) {
		System.out.println("         Adding Worker Component to Model");
		Component worker = pFactory.createComponent();
		worker.setTitle("hWorker");
		// worker.setOcciComponentState(Status.ACTIVE);
		config.getResources().add(worker);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(worker.getId());
		worker.getAttributes().add(attr);

		MixinBase mBase = factory.createMixinBase();
		mBase.setMixin(getMixin("hadoop-worker", config));
		worker.getParts().add(mBase);

		Placementlink pLink = placeFactory.createPlacementlink();
		pLink.setSource(worker);
		pLink.setTarget(comp);

		Componentlink compLink = pFactory.createComponentlink();
		compLink.setSource(getResourceById(config.getResources(), getApplicationId(config)));
		compLink.setTarget(worker);

		Componentsim sim = WorkloadFactory.eINSTANCE.createComponentsim();
		worker.getParts().add(sim);

		MAPE.newComp = worker;
		return worker;
	}

	private String getApplicationId(Configuration config) {
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res instanceof Application) {
				if (res.getTitle().toLowerCase().contains("hadoop")) {
					return res.getId();
				}
			}
		}
		return "";
	}

	private Sensor addSensor(Configuration config, Compute comp) {
		System.out.println("         Adding Sensor to Model");
		Sensor sens = mFactory.createSensor();
		sens.setTitle("sensor");
		AttributeState attrName = factory.createAttributeState();
		attrName.setName("occi.app.name");
		attrName.setValue("sensor");
		sens.getAttributes().add(attrName);

		AttributeState attrContext = factory.createAttributeState();
		attrContext.setName("occi.app.context");
		attrContext.setValue("http://www.de");
		sens.getAttributes().add(attrContext);

		AttributeState attrUrl = factory.createAttributeState();
		attrUrl.setName("occi.app.url");
		attrUrl.setValue("http://www.de");
		sens.getAttributes().add(attrUrl);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(sens.getId());
		sens.getAttributes().add(attr);

		Monitorableproperty mp = mFactory.createMonitorableproperty();
		mp.setTitle("monProp");
		mp.setMonitoringProperty("CPU");
		AttributeState attrProp = factory.createAttributeState();
		attrProp.setName("monitoring.property");
		attrProp.setValue("CPU");
		mp.getAttributes().add(attrProp);
		mp.setSource(sens);
		mp.setTarget(comp);

		Sensorsim sim = WorkloadFactory.eINSTANCE.createSensorsim();
		String simString = "None, None, High, Critical";
		sim.setSimChangeRate(3000);
		sim.setSimMonitoringResults(simString);
		AttributeState attrRes = factory.createAttributeState();
		attrRes.setName("sim.monitoring.results");
		attrRes.setValue(simString);
		sim.getAttributes().add(attrRes);
		sens.getParts().add(sim);

		config.getResources().add(sens);

		return sens;
	}

	private Datagatherer addDataGatherer(Configuration config, Compute comp, Sensor sens) {
		System.out.println("             Adding Datagatherer to Model");
		Datagatherer dg = mFactory.createDatagatherer();
		dg.setTitle("CPUGatherer");
		config.getResources().add(dg);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(dg.getId());
		dg.getAttributes().add(attr);

		MixinBase mBase = factory.createMixinBase();
		mBase.setMixin(getMixin("cpugatherer", config));
		dg.getParts().add(mBase);

		Componentlink c1 = pFactory.createComponentlink();
		c1.setSource(sens);
		c1.setTarget(dg);

		Placementlink pl = placeFactory.createPlacementlink();
		pl.setSource(dg);
		pl.setTarget(comp);

		return dg;
	}

	private Dataprocessor addDataProcessor(Configuration config, Compute comp, Sensor sens, Datagatherer dg) {
		System.out.println("             Adding Dataprocessor to Model");
		Dataprocessor dp = mFactory.createDataprocessor();
		dp.setTitle("CPUProcessor");
		config.getResources().add(dp);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(dp.getId());
		dp.getAttributes().add(attr);

		MixinBase mBase = factory.createMixinBase();
		mBase.setMixin(getMixin("cpuprocessorlocal", config));
		dp.getParts().add(mBase);

		Componentlink c2 = pFactory.createComponentlink();
		c2.setSource(sens);
		c2.setTarget(dp);
		Placementlink pl = placeFactory.createPlacementlink();
		pl.setSource(dp);
		pl.setTarget(comp);
		Componentlink cl = pFactory.createComponentlink();
		cl.setSource(dp);
		cl.setTarget(dg);
		return dp;
	}

	private Resultprovider addResultProvider(Configuration config, Compute comp, Sensor sens) {
		System.out.println("             Adding Resultprovider to Model");
		Resultprovider rp = mFactory.createResultprovider();
		// rp.setTitle("CPUProvider");
		config.getResources().add(rp);

		Occiresultprovider orp = mFactory.createOcciresultprovider();
		orp.setResultProviderEndpoint("localhost:8080");
		AttributeState attrOP = factory.createAttributeState();
		attrOP.setName("result.provider.endpoint");
		attrOP.setValue("localhost:8080");
		rp.getAttributes().add(attrOP);

		rp.getParts().add(orp);

		MixinBase mBase = factory.createMixinBase();
		mBase.setMixin(getMixin("cpupublisher", config));
		rp.getParts().add(mBase);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(rp.getId());
		rp.getAttributes().add(attr);

		Componentlink c3 = pFactory.createComponentlink();
		c3.setSource(sens);
		c3.setTarget(rp);

		Placementlink pl = placeFactory.createPlacementlink();
		pl.setSource(rp);
		pl.setTarget(comp);

		Componentsim sim = WorkloadFactory.eINSTANCE.createComponentsim();
		rp.getParts().add(sim);

		return rp;
	}

	private Mixin getMixin(String string, Configuration config) {
		for (Mixin mix : config.getMixins()) {
			if (mix.getTerm().equals(string)) {
				return mix;
			}
		}
		return null;
	}
}
