/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Path;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;

import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.connector.MartConnector;
import de.ugoe.cs.rwm.docci.executor.MartExecutor;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import de.ugoe.cs.rwm.tocci.occi2openstack.OCCI2OPENSTACKTransformator;

public class InitialDeployment {

	public static void main(String args[]) {
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		// Connector conn = new MartConnector("192.168.35.45", 8080, "ubuntu",
		// "~/key.pem");
		RegistryAndLoggerSetup.setup();
		Path occiPath;

		if (args.length == 0) {
			System.out.println("Choosing default initial deployment model");
			occiPath = getModelPath("de/ugoe/cs/rwm/mocci/occi/hadoopClusterCPU.occic");
		} else {
			System.out.println("Choosing user defined deployment model: " + args[0]);
			File occiFile = new File(args[0]);
			occiPath = occiFile.toPath().toAbsolutePath();
		}

		Resource model = ModelUtility.loadOCCIintoEMFResource(occiPath);

		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		try {
			trans.transform(model, occiPath);
		} catch (EolRuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model = ModelUtility.loadOCCIintoEMFResource(occiPath);

		OCCI2OPENSTACKTransformator trans2 = new OCCI2OPENSTACKTransformator();
		trans2.setTransformationProperties(RegistryAndLoggerSetup.manNWRuntimeId, RegistryAndLoggerSetup.sshKey,
				RegistryAndLoggerSetup.userData, RegistryAndLoggerSetup.manNWid);

		try {
			trans2.transform(model, occiPath);
		} catch (EolRuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model = ModelUtility.loadOCCIintoEMFResource(occiPath);

		MartDeployer deployer;
		if (conn instanceof MartConnector) {
			deployer = new MartDeployer(conn, 100000);
		} else {
			deployer = new MartDeployer(conn);
		}
		deployer.deploy(model);
		MartExecutor executor = new MartExecutor(conn);
		executor.executeGetOperation("/resultprovider");
	}

	public static Path getModelPath(String resourceName) {
		File file = null;
		URL res = ClassLoader.getSystemClassLoader().getResource(resourceName);
		if (res.toString().startsWith("jar:")) {
			try (InputStream input = ClassLoader.getSystemClassLoader().getResourceAsStream(resourceName)) {
				file = File.createTempFile("tempfile", ".occic");
				@SuppressWarnings("resource")
				OutputStream out = new FileOutputStream(file);
				int read;
				int byteArrSize = 1024;
				byte[] bytes = new byte[byteArrSize];

				while ((read = input.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}
				file.deleteOnExit();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} else {
			// this will probably work in your IDE, but not from a JAR
			file = new File(res.getFile());
		}

		if (file != null && !file.exists()) {
			throw new RuntimeException("Error: File " + file + " not found!");
		}
		return file.toPath();
	}
}
