/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci;

public class Monitor {
	private int critCPUs;
	private int noneCPUs;
	private int allCPUs;

	public int getAllCPUs() {
		return allCPUs;
	}

	public void setAllCPUs(int allCPUs) {
		this.allCPUs = allCPUs;
	}

	public Monitor(int critCPUs, int noneCPUs, int allCPUs) {
		this.critCPUs = critCPUs;
		this.noneCPUs = noneCPUs;
		this.allCPUs = allCPUs;
	}

	public int getCritCPUs() {
		return critCPUs;
	}

	public void setCritCPUs(int critCPUs) {
		this.critCPUs = critCPUs;
	}

	public int getNoneCPUs() {
		return noneCPUs;
	}

	public void setNoneCPUs(int noneCPUs) {
		this.noneCPUs = noneCPUs;
	}

}
