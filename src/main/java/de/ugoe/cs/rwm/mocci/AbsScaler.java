/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.cmf.occi.core.impl.OCCIFactoryImpl;
import org.eclipse.cmf.occi.infrastructure.impl.InfrastructureFactoryImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.modmacao.ansibleconfiguration.impl.AnsibleconfigurationFactoryImpl;
import org.modmacao.occi.platform.impl.PlatformFactoryImpl;
import org.modmacao.placement.impl.PlacementFactoryImpl;

import de.ugoe.cs.rwm.docci.connector.Connector;
import monitoring.impl.MonitoringFactoryImpl;
import ossweruntime.impl.OssweruntimeFactoryImpl;

public abstract class AbsScaler {
	protected InfrastructureFactoryImpl iFactory = new InfrastructureFactoryImpl();
	protected OCCIFactoryImpl factory = new OCCIFactoryImpl();
	protected MonitoringFactoryImpl mFactory = new MonitoringFactoryImpl();
	protected PlatformFactoryImpl pFactory = new PlatformFactoryImpl();
	protected PlacementFactoryImpl placeFactory = new PlacementFactoryImpl();
	protected OssweruntimeFactoryImpl osFactory = new OssweruntimeFactoryImpl();
	protected AnsibleconfigurationFactoryImpl aFactory = new AnsibleconfigurationFactoryImpl();
	protected Resource runtimeModel;
	protected Connector conn;
	protected Path runtimePath;

	@SuppressWarnings("serial")
	static List<String> interfaces = new ArrayList<String>() {
		{
			add("10.254.1.12");
			add("10.254.1.22");
			add("10.254.1.32");
			add("10.254.1.42");
			add("10.254.1.52");
		}
	};

	protected org.eclipse.cmf.occi.core.Resource getResourceById(EList<org.eclipse.cmf.occi.core.Resource> eList,
			String string) {
		for (org.eclipse.cmf.occi.core.Resource res : eList) {
			if (res.getId().equals(string)) {
				return res;
			}
		}
		return null;
	}
}
