package de.ugoe.cs.rwm.mocci;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.junit.BeforeClass;
import org.junit.Test;

public class ResolveProxyTest {

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Test
	public void resolveProxies() {
		Path newOCCI = Paths
				.get(de.ugoe.cs.rwm.docci.ModelUtility.getPathToResource("occi/hadoopClusterNewExtWithMem.occic"));

		Resource ress = de.ugoe.cs.rwm.docci.ModelUtility.loadOCCIintoEMFResource(newOCCI);
		@SuppressWarnings("unused")
		ResourceSet resSet = ress.getResourceSet();

		EList<EObject> newModel = de.ugoe.cs.rwm.docci.ModelUtility.loadOCCI(newOCCI);

		for (org.eclipse.cmf.occi.core.Resource res : de.ugoe.cs.rwm.docci.ModelUtility.getResources(newModel)) {
			System.out.println("Resource Title: " + res.getTitle());
			System.out.println("      Kind: " + res.getKind());
			assertTrue(!(res.getKind().eIsProxy()));
			for (Link link : res.getLinks()) {
				System.out.println("      Link: " + link.getTitle());
				System.out.println("      " + "      Kind:" + link.getKind());
				System.out.println("      " + "      Target:" + link.getTarget());
				assertTrue(!(link.getKind().eIsProxy()));
			}
			for (Mixin mix : res.getMixins()) {
				System.out.println("      " + mix);
				System.out.println("      " + mix.getScheme());
				assertTrue(!(mix.eIsProxy()));
			}
		}
	}
}
