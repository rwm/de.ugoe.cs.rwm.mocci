/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package monitoring.impl;

import monitoring.Dataprocessor;
import monitoring.MonitoringPackage;

import org.eclipse.emf.ecore.EClass;

import org.modmacao.occi.platform.impl.ComponentImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dataprocessor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DataprocessorImpl extends ComponentImpl implements Dataprocessor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataprocessorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MonitoringPackage.Literals.DATAPROCESSOR;
	}

} //DataprocessorImpl
