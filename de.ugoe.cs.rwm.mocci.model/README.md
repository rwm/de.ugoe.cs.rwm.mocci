# Monitoring Extension
This component represents the OCCI monitoring extension, we generated using [OCCI-Studio](https://github.com/occiware/OCCI-Studio).
The elements introduced in this extension mainly inherit from elements of the enhanced platform extension of [MoDMaCAO](https://github.com/occiware/MoDMaCAO),
as shown in the Figure below. As any other extension generated with OCCI-Studio the monitoring extension can be [registered as a plugin](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/blob/master/doc/studio.md) within Eclipse, OCCI-Studio, and the MartServer.

## The Sensor Element
The Sensor element represents a top level element used to easily manage all monitoring devices it contains.
The containment relationship to its monitoring devices is modeled via ComponentLinks.
As Sensor inherits Application, it gains access to attributes reflecting its current state, as well as actions to manage its lifecycle.
Thus, the monitoring devices can be deployed by accessing the sensor directly, but also by addressing each device separately.

To specify the object a sensor monitors, it can be connected over a MonitorableProperty to the corresponding resource.
The property itself defines two attributes defining the name of the property it monitors, and its results.

## The Monitoring Components
Each monitoring device (DataGatherer, DataProcessor, ResultProvider) inherit from component. 
Thus, each monitoring component gains access to actions to deploy, configure, start, stop, and undeploy it.
When not using the dummy connector, the execution of these actions trigger the execution of configuration management scripts linked to the individual monitoring device.
The name of the scripts are assigned over user Mixins attached to the Component, which is located in the roles folder of the MartServer.
To define where each monitoring device is deployed PlacementLinks are used connecting the device to the VM it is deployed on.
Thus, each device can be hosted by different VM.

![Ext](./monExt.jpg "Ext")