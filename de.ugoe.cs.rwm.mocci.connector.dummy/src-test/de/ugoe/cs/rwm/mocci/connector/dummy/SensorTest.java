package de.ugoe.cs.rwm.mocci.connector.dummy;

import static org.junit.Assert.assertTrue;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.InfrastructureFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.Componentlink;
import org.modmacao.occi.platform.PlatformFactory;
import org.modmacao.occi.platform.Status;

import de.ugoe.cs.rwm.mocci.connector.ConnectorFactory;
import monitoring.Datagatherer;
import monitoring.Dataprocessor;
import monitoring.Monitorableproperty;
import monitoring.Resultprovider;
import monitoring.Sensor;

public class SensorTest {
	ConnectorFactory fac = new ConnectorFactory();
	PlatformFactory pFac = PlatformFactory.eINSTANCE;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
	}

	@Test
	public void startSensorLifecycleToStart() {
		Sensor sens = createSensor(Status.UNDEPLOYED);
		sens.deploy();
		assertTrue(inState(sens, Status.DEPLOYED));
		sens.configure();
		assertTrue(inState(sens, Status.INACTIVE));
		sens.start();
		assertTrue(inState(sens, Status.ACTIVE));
	}

	@Test
	public void startSensorLifecycleToUndeploy() {
		Sensor sens = createSensor(Status.ACTIVE);
		sens.stop();
		assertTrue(inState(sens, Status.INACTIVE));
		sens.undeploy();
		assertTrue(inState(sens, Status.UNDEPLOYED));
	}

	@Test
	public void stopSensorActiveToUndeploy() {
		Sensor sens = createSensor(Status.ACTIVE);
		sens.undeploy();
		assertTrue(inState(sens, Status.UNDEPLOYED));
	}

	@Test
	public void stopSensorUndeployToStart() {
		Sensor sens = createSensor(Status.UNDEPLOYED);
		sens.start();
		assertTrue(inState(sens, Status.ACTIVE));
	}

	@Test
	public void startSensorDeployToUndeploy() {
		Sensor sens = createSensor(Status.DEPLOYED);
		sens.undeploy();
		assertTrue(inState(sens, Status.UNDEPLOYED));
	}

	@Test
	public void occiSensorRequests() {
		Sensor sens = createSensor(Status.UNDEPLOYED);
		sens.occiCreate();
		sens.occiRetrieve();
		sens.occiUpdate();
		sens.occiDelete();
		assertTrue(true);
	}

	private Boolean inState(Sensor sens, Status state) {
		System.out.println(sens.getOcciAppState());
		if (sens.getOcciAppState().getValue() != state.getValue()) {
			return false;
		}
		for (Link link : sens.getLinks()) {
			if (link instanceof Componentlink) {
				Component comp = (Component) link.getTarget();
				System.out.println(comp.getOcciComponentState());
				if ((comp.getOcciComponentState().getValue() == state.getValue()) == false) {
					return false;
				}
			}
		}
		return true;
	}

	private Sensor createSensor(Status state) {
		Sensor sens = fac.createSensor();
		sens.setOcciAppState(state);

		Resultprovider rp = fac.createResultprovider();
		rp.setOcciComponentState(state);
		rp.setOcciComponentState(state);
		Componentlink cLink = pFac.createComponentlink();
		cLink.setSource(sens);
		cLink.setTarget(rp);

		Compute vm = InfrastructureFactory.eINSTANCE.createCompute();
		Monitorableproperty monProp = fac.createMonitorableproperty();
		monProp.setSource(sens);
		monProp.setTarget(vm);
		monProp.setMonitoringProperty("CPU");

		Datagatherer dg = fac.createDatagatherer();
		dg.setOcciComponentState(state);
		Componentlink cLink2 = pFac.createComponentlink();
		cLink2.setSource(sens);
		cLink2.setTarget(dg);

		Dataprocessor dp = fac.createDataprocessor();
		dp.setOcciComponentState(state);
		Componentlink cLink3 = pFac.createComponentlink();
		cLink3.setSource(sens);
		cLink3.setTarget(dp);

		return sens;

	}
}
