package de.ugoe.cs.rwm.mocci.connector.dummy;

import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.InfrastructureFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.occi.platform.Componentlink;
import org.modmacao.occi.platform.PlatformFactory;
import org.modmacao.occi.platform.Status;

import de.ugoe.cs.rwm.mocci.connector.ConnectorFactory;
import monitoring.Datagatherer;
import monitoring.Monitorableproperty;
import monitoring.Sensor;

public class DatagathererTest {
	ConnectorFactory fac = new ConnectorFactory();
	PlatformFactory pFac = PlatformFactory.eINSTANCE;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();

	}

	@Test
	public void occiCreateDatagatherer() {
		Datagatherer dg = fac.createDatagatherer();
		dg.occiCreate();
		assertTrue(true);
	}

	@Test
	public void getDatagathererInactive() {
		Datagatherer dg = createDatagatherer(Status.UNDEPLOYED);
		Monitorableproperty monProp = getMonProp(getSensor(dg));
		assertTrue(monProp.getMonitoringResult() == null);
		dg.occiRetrieve();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertTrue(monProp.getMonitoringResult() == null);

	}

	@Test
	public void DatagathererLifecycleToStart() {
		Datagatherer dg = createDatagatherer(Status.UNDEPLOYED);
		dg.deploy();
		assertTrue(inState(dg, Status.DEPLOYED));
		dg.configure();
		assertTrue(inState(dg, Status.INACTIVE));
		dg.start();
		assertTrue(inState(dg, Status.ACTIVE));
	}

	@Test
	public void DatagathererLifecycleToUndeployed() {
		Datagatherer dg = createDatagatherer(Status.ACTIVE);
		dg.stop();
		assertTrue(inState(dg, Status.INACTIVE));
		dg.undeploy();
		assertTrue(inState(dg, Status.UNDEPLOYED));

		Datagatherer rp2 = createDatagatherer(Status.DEPLOYED);
		rp2.undeploy();
		assertTrue(inState(dg, Status.UNDEPLOYED));
	}

	@Test
	public void DatagathererLifecycleDeployToUndeployed() {
		Datagatherer dg = createDatagatherer(Status.DEPLOYED);
		dg.undeploy();
		assertTrue(inState(dg, Status.UNDEPLOYED));
	}

	@Test
	public void DatagathererLifecycleActiveToUndeploy() {
		Datagatherer dg = createDatagatherer(Status.UNDEPLOYED);
		dg.start();
		dg.undeploy();
		assertTrue(inState(dg, Status.UNDEPLOYED));
	}

	@Test
	public void DatagathererLifecycleActiveToStop() {
		Datagatherer dg = createDatagatherer(Status.UNDEPLOYED);
		dg.start();
		dg.stop();
		assertTrue(inState(dg, Status.INACTIVE));
		dg.start();
		assertTrue(inState(dg, Status.ACTIVE));
	}

	@Test
	public void occiDatagathererRequests() {
		Datagatherer dg = createDatagatherer(Status.UNDEPLOYED);
		dg.occiCreate();
		dg.occiRetrieve();
		dg.occiUpdate();
		dg.occiDelete();
		assertTrue(true);
	}

	private Boolean inState(Datagatherer dg, Status deployed) {
		return dg.getOcciComponentState().getValue() == deployed.getValue();
	}

	private Sensor getSensor(Datagatherer dg) {
		for (Link link : dg.getRlinks()) {
			if (link.getSource() instanceof Sensor) {
				return ((Sensor) link.getSource());
			}
		}
		throw new NoSuchElementException("No containing sensor found!");
	}

	private Monitorableproperty getMonProp(Sensor sensor) {
		for (Link link : sensor.getLinks()) {
			if (link instanceof Monitorableproperty) {
				return ((Monitorableproperty) link);
			}
		}
		throw new NoSuchElementException("No monitorableproperty found in sensor!");
	}

	private Datagatherer createDatagatherer(Status state) {
		Sensor sens = fac.createSensor();

		Datagatherer dg = fac.createDatagatherer();
		dg.setOcciComponentState(state);
		Componentlink cLink = pFac.createComponentlink();
		cLink.setSource(sens);
		cLink.setTarget(dg);

		Compute vm = InfrastructureFactory.eINSTANCE.createCompute();
		Monitorableproperty monProp = fac.createMonitorableproperty();
		monProp.setSource(sens);
		monProp.setTarget(vm);
		monProp.setMonitoringProperty("CPU");

		return dg;

	}

}
