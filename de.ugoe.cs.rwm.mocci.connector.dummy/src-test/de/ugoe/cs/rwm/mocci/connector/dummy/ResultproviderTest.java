package de.ugoe.cs.rwm.mocci.connector.dummy;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.NoSuchElementException;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.InfrastructureFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.occi.platform.Componentlink;
import org.modmacao.occi.platform.PlatformFactory;
import org.modmacao.occi.platform.Status;

import de.ugoe.cs.rwm.mocci.connector.ConnectorFactory;
import de.ugoe.cs.rwm.mocci.connector.ResultproviderConnector;
import monitoring.Monitorableproperty;
import monitoring.Occiresultprovider;
import monitoring.Resultprovider;
import monitoring.Sensor;

public class ResultproviderTest {
	ConnectorFactory fac = new ConnectorFactory();
	PlatformFactory pFac = PlatformFactory.eINSTANCE;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();

	}

	@Test(expected = NoSuchElementException.class)
	public void startSimulationWithoutSensor() {
		Resultprovider rp = fac.createResultprovider();
		rp.start();
	}

	@Test
	public void startSimulationWithSensor() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		Monitorableproperty monProp = getMonProp(getSensor(rp));
		assertTrue(monProp.getMonitoringResult() == null);
		rp.start();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertFalse(monProp.getMonitoringResult().contains(","));
	}

	@Test
	public void startSimulationWithSensorQuotes() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		Monitorableproperty monProp = getMonProp(getSensor(rp));
		monProp.setMonitoringProperty("MecoTaskResultForEach");
		assertTrue(monProp.getMonitoringResult() == null);
		rp.start();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertTrue(monProp.getMonitoringResult().contains(","));
	}

	@Test
	public void occiCreateResultprovider() {
		Resultprovider rp = fac.createResultprovider();
		rp.occiCreate();
		assertTrue(true);
	}

	@Test
	public void getResultproviderActiveState() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		rp.setOcciComponentState(Status.ACTIVE);
		Monitorableproperty monProp = getMonProp(getSensor(rp));
		assertTrue(monProp.getMonitoringResult() == null);
		rp.occiRetrieve();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertTrue(monProp.getMonitoringResult() != null);

	}

	@Test
	public void getResultproviderInactive() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		Monitorableproperty monProp = getMonProp(getSensor(rp));
		assertTrue(monProp.getMonitoringResult() == null);
		rp.occiRetrieve();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertTrue(monProp.getMonitoringResult() == null);

	}

	@Test
	public void stopResultproviderInactive() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		rp.start();
		Thread t = getSimulationThread(rp);
		assertTrue(t.isAlive());
		rp.occiDelete();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertFalse(t.isAlive());
	}

	@Test
	public void ResultproviderLifecycleToStart() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		rp.deploy();
		assertTrue(inState(rp, Status.DEPLOYED));
		rp.configure();
		assertTrue(inState(rp, Status.INACTIVE));
		rp.start();
		assertTrue(inState(rp, Status.ACTIVE));
	}

	@Test
	public void ResultproviderLifecycleToUndeployed() {
		Resultprovider rp = createResultprovider(Status.ACTIVE);
		rp.stop();
		assertTrue(inState(rp, Status.INACTIVE));
		rp.undeploy();
		assertTrue(inState(rp, Status.UNDEPLOYED));

		Resultprovider rp2 = createResultprovider(Status.DEPLOYED);
		rp2.undeploy();
		assertTrue(inState(rp, Status.UNDEPLOYED));
	}

	@Test
	public void ResultproviderLifecycleDeployToUndeployed() {
		Resultprovider rp = createResultprovider(Status.DEPLOYED);
		rp.undeploy();
		assertTrue(inState(rp, Status.UNDEPLOYED));
	}

	@Test
	public void ResultproviderLifecycleActiveToUndeploy() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		rp.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread t = getSimulationThread(rp);
		assertTrue(t.isAlive());

		rp.undeploy();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertFalse(t.isAlive());
		assertTrue(inState(rp, Status.UNDEPLOYED));
	}

	@Test
	public void ResultproviderLifecycleActiveToStop() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		rp.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread t = getSimulationThread(rp);
		assertTrue(t.isAlive());

		rp.stop();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertFalse(t.isAlive());
		assertTrue(inState(rp, Status.INACTIVE));
		rp.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread newT = getSimulationThread(rp);
		assertTrue(newT.isAlive());
		assertTrue(inState(rp, Status.ACTIVE));
	}

	@Test
	public void occiRequestsResultprovider() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		rp.occiCreate();
		rp.occiRetrieve();
		rp.occiUpdate();
		rp.occiDelete();
		assertTrue(true);
	}

	@Test
	public void occiResultproviderRequests() {
		Resultprovider rp = createResultprovider(Status.UNDEPLOYED);
		Occiresultprovider orp = fac.createOcciresultprovider();
		rp.getParts().add(orp);
		assertTrue(true);
	}

	private Boolean inState(Resultprovider rp, Status deployed) {
		return rp.getOcciComponentState().getValue() == deployed.getValue();
	}

	private Thread getSimulationThread(Resultprovider rp) {
		Class<ResultproviderConnector> rpc = ResultproviderConnector.class;
		Field f = null;
		try {
			f = rpc.getDeclaredField("simulation");
			f.setAccessible(true);
			return (Thread) f.get(rp);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			assertTrue(false);
		}
		throw new NoSuchElementException("Simulation thread could not be found!");
	}

	private Sensor getSensor(Resultprovider rp) {
		for (Link link : rp.getRlinks()) {
			if (link.getSource() instanceof Sensor) {
				return ((Sensor) link.getSource());
			}
		}
		throw new NoSuchElementException("No containing sensor found!");
	}

	private Monitorableproperty getMonProp(Sensor sensor) {
		for (Link link : sensor.getLinks()) {
			if (link instanceof Monitorableproperty) {
				return ((Monitorableproperty) link);
			}
		}
		throw new NoSuchElementException("No monitorableproperty found in sensor!");
	}

	private Resultprovider createResultprovider(Status state) {
		Sensor sens = fac.createSensor();

		Resultprovider rp = fac.createResultprovider();
		rp.setOcciComponentState(state);
		Componentlink cLink = pFac.createComponentlink();
		cLink.setSource(sens);
		cLink.setTarget(rp);

		Compute vm = InfrastructureFactory.eINSTANCE.createCompute();
		Monitorableproperty monProp = fac.createMonitorableproperty();
		monProp.setSource(sens);
		monProp.setTarget(vm);
		monProp.setMonitoringProperty("CPU");

		return rp;

	}

}
