package de.ugoe.cs.rwm.mocci.connector.dummy;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.occi.platform.PlatformFactory;

import de.ugoe.cs.rwm.mocci.connector.ConnectorFactory;
import monitoring.Monitorableproperty;

public class MonitorablepropertyTest {
	ConnectorFactory fac = new ConnectorFactory();
	PlatformFactory pFac = PlatformFactory.eINSTANCE;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();

	}

	@Test
	public void occiDataprocessorRequests() {
		Monitorableproperty monprop = fac.createMonitorableproperty();
		monprop.occiCreate();
		monprop.occiRetrieve();
		monprop.occiUpdate();
		monprop.occiDelete();
		assertTrue(true);
	}
}
