package de.ugoe.cs.rwm.mocci.connector.dummy;

import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.InfrastructureFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.occi.platform.Componentlink;
import org.modmacao.occi.platform.PlatformFactory;
import org.modmacao.occi.platform.Status;

import de.ugoe.cs.rwm.mocci.connector.ConnectorFactory;
import monitoring.Dataprocessor;
import monitoring.Monitorableproperty;
import monitoring.Sensor;

public class DataprocessorTest {
	ConnectorFactory fac = new ConnectorFactory();
	PlatformFactory pFac = PlatformFactory.eINSTANCE;

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();

	}

	@Test
	public void occiCreateDataprocessor() {
		Dataprocessor dp = fac.createDataprocessor();
		dp.occiCreate();
		assertTrue(true);
	}

	@Test
	public void getDataprocessorInactive() {
		Dataprocessor dp = createDataprocessor(Status.UNDEPLOYED);
		Monitorableproperty monProp = getMonProp(getSensor(dp));
		assertTrue(monProp.getMonitoringResult() == null);
		dp.occiRetrieve();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertTrue(monProp.getMonitoringResult() == null);

	}

	@Test
	public void DataprocessorLifecycleToStart() {
		Dataprocessor dp = createDataprocessor(Status.UNDEPLOYED);
		dp.deploy();
		assertTrue(inState(dp, Status.DEPLOYED));
		dp.configure();
		assertTrue(inState(dp, Status.INACTIVE));
		dp.start();
		assertTrue(inState(dp, Status.ACTIVE));
	}

	@Test
	public void DataprocessorLifecycleToUndeployed() {
		Dataprocessor dp = createDataprocessor(Status.ACTIVE);
		dp.stop();
		assertTrue(inState(dp, Status.INACTIVE));
		dp.undeploy();
		assertTrue(inState(dp, Status.UNDEPLOYED));

		Dataprocessor rp2 = createDataprocessor(Status.DEPLOYED);
		rp2.undeploy();
		assertTrue(inState(dp, Status.UNDEPLOYED));
	}

	@Test
	public void DataprocessorLifecycleDeployToUndeployed() {
		Dataprocessor dp = createDataprocessor(Status.DEPLOYED);
		dp.undeploy();
		assertTrue(inState(dp, Status.UNDEPLOYED));
	}

	@Test
	public void DataprocessorLifecycleActiveToUndeploy() {
		Dataprocessor dp = createDataprocessor(Status.UNDEPLOYED);
		dp.start();
		dp.undeploy();
		assertTrue(inState(dp, Status.UNDEPLOYED));
	}

	@Test
	public void DataprocessorLifecycleActiveToStop() {
		Dataprocessor dp = createDataprocessor(Status.UNDEPLOYED);
		dp.start();
		dp.stop();
		assertTrue(inState(dp, Status.INACTIVE));
		dp.start();
		assertTrue(inState(dp, Status.ACTIVE));
	}

	@Test
	public void occiDataprocessorRequests() {
		Dataprocessor dp = createDataprocessor(Status.UNDEPLOYED);
		dp.occiCreate();
		dp.occiRetrieve();
		dp.occiUpdate();
		dp.occiDelete();
		assertTrue(true);
	}

	private Boolean inState(Dataprocessor dp, Status deployed) {
		return dp.getOcciComponentState().getValue() == deployed.getValue();
	}

	private Sensor getSensor(Dataprocessor dp) {
		for (Link link : dp.getRlinks()) {
			if (link.getSource() instanceof Sensor) {
				return ((Sensor) link.getSource());
			}
		}
		throw new NoSuchElementException("No containing sensor found!");
	}

	private Monitorableproperty getMonProp(Sensor sensor) {
		for (Link link : sensor.getLinks()) {
			if (link instanceof Monitorableproperty) {
				return ((Monitorableproperty) link);
			}
		}
		throw new NoSuchElementException("No monitorableproperty found in sensor!");
	}

	private Dataprocessor createDataprocessor(Status state) {
		Sensor sens = fac.createSensor();

		Dataprocessor dp = fac.createDataprocessor();
		dp.setOcciComponentState(state);
		Componentlink cLink = pFac.createComponentlink();
		cLink.setSource(sens);
		cLink.setTarget(dp);

		Compute vm = InfrastructureFactory.eINSTANCE.createCompute();
		Monitorableproperty monProp = fac.createMonitorableproperty();
		monProp.setSource(sens);
		monProp.setTarget(vm);
		monProp.setMonitoringProperty("CPU");

		return dp;

	}

}
