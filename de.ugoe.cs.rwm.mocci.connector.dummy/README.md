# Monitoring Dummy Connector
Each element in the monitoring extension has a single file in the connector implementing how to react on different REST requests addressing the corresponding OCCI element.
This dummy is especially useful for testing purposes. Compared to the [connector](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/tree/master/de.ugoe.cs.rwm.mocci.connector) for an actual deployment of sensors, a link to configuration management scripts is not necessary in order to get monitoring data.
As soon as a resultprovider is started monitoring results are simulated are passed to the monitorableproperty element of the sensor containing the resultprovider.
The simulated values are easy to adjust and does not require a recompilation of the connector, as only the resultprovider.properties file within the connector has to be modified.
The skeleton for the connector of the OCCI monitoring extension is generated using [OCCI-Studio](https://github.com/occiware/OCCI-Studio).


## Adjusting the Simulation
To configure the simulation of monitoring results the following steps have to be performed.
1. Navigate to ~/martserver-plugins
2. Open the de.ugoe.cs.rwm.mocci.connecter.dummy.jar with an archive manager.
3. Doubleclick on the resultprovider.properties file
4. Adjust the values to your liking

The adjustment of the file itself is best explained with a small example:
As default the file contains the following:
```
CPU = None,Low,Medium,High,Critical,5000
```

Hereby, the following information can be adjusted:
* The first string (CPU): represents the monitorable.property to be adjusted by the simulation.
* The last integer (5000): represents the interval in milliseconds in which a new monitoring.result is written.
* Every string between (None-Critical): represents the values to be filled into the monitoring.result property.

For example, to additionally generate monitoring results for MonitorableProperties having its monitoring.property set to Mem, the file could look like this:
```
CPU = None,Low,Medium,High,Critical,5000
Mem = Save,Alert,10000
```
