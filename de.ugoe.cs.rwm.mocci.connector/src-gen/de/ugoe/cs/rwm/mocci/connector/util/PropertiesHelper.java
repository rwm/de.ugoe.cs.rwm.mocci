/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci.connector.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertiesHelper {
	Properties props;
	private final static String FILENAME = "mocci.properties";

	public PropertiesHelper() {
		loadProperties();
	}

	/**
	 * Getter method for providing the properties of this ResultProviderHelper.
	 * Properties will be read from local file resultprovider.properties.
	 *
	 * @return The properties
	 */
	public Properties getProperties() {
		if (props == null) {
			loadProperties();
		}

		return props;
	}

	private void loadProperties() {
		props = new Properties();
		InputStream input = null;

		try {
			input = this.getClass().getClassLoader().getResourceAsStream(FILENAME);
			props.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
