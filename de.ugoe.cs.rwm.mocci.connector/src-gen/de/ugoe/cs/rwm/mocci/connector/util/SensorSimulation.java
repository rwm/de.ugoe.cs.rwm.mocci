package de.ugoe.cs.rwm.mocci.connector.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.impl.OCCIFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ugoe.cs.rwm.mocci.connector.SensorConnector;
import monitoring.Monitorableproperty;
import monitoring.Sensor;

public class SensorSimulation implements Runnable {
	private static Logger LOGGER = LoggerFactory.getLogger(SensorSimulation.class);
	private Monitorableproperty monProp;
	private OCCIFactoryImpl factory = new OCCIFactoryImpl();
	private int interval;
	private List<String> results;
	private MixinBase simMixing;

	public SensorSimulation(SensorConnector sensor) {
		this.simMixing = sensor.getSimulationMixin();
		this.monProp = getMonProp(sensor);
		String property = getAttribute(simMixing, "sim.monitoring.results");
		LOGGER.info("Monitoring property for" + monProp.getMonitoringProperty() + ": " + property);
		List<String> items = new ArrayList<String>(Arrays.asList(property.split("\\s*,\\s*")));
		this.interval = Integer.parseInt(getAttribute(simMixing, "sim.change.rate"));

		String itemsString = String.join(",", items);
		Pattern p = Pattern.compile("\'([^\']*)\'");
		Matcher m = p.matcher(itemsString);
		if (m.find() == true) {
			items.clear();
			while (m.find()) {
				items.add(m.group(0));
			}
		}

		this.results = items;
		LOGGER.info("Creating Simulation for: " + monProp.getMonitoringProperty() + "; with values: " + results
				+ "; and interval: " + interval);
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			AttributeState monPropAttr = factory.createAttributeState();
			monPropAttr.setName("monitoring.result");
			int randomElementIndex = ThreadLocalRandom.current().nextInt(results.size());
			String value = results.get(randomElementIndex);
			monPropAttr.setValue(value);
			monProp.setMonitoringResult(value);
			LOGGER.info(
					"MonProp: " + monProp.getMonitoringProperty() + ", set to: " + value + "(" + monProp.getId() + ")");
			monProp.getAttributes().add(monPropAttr);
		}

	}

	private Monitorableproperty getMonProp(Sensor sensor) {
		for (Link link : sensor.getLinks()) {
			if (link instanceof Monitorableproperty) {
				return ((Monitorableproperty) link);
			}
		}
		throw new NoSuchElementException("No monitorableproperty found in sensor!");
	}

	public static String getAttribute(MixinBase simMixing, String s) {
		for (AttributeState attr : simMixing.getAttributes()) {
			if (attr.getName().equals(s)) {
				return attr.getValue();
			}
		}
		return "";
	}
}
