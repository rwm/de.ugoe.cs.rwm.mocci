package de.ugoe.cs.rwm.mocci.connector;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.modmacao.cm.ConfigurationManagementTool;
import org.modmacao.cm.ansible.AnsibleCMTool;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.Status;

import modmacao.Executiondependency;
import modmacao.Installationdependency;

public class ComponentManager {

	private ConfigurationManagementTool cmtool = new AnsibleCMTool();
	private Component comp;

	public ComponentManager(Component comp) {
		this.comp = comp;
	}

	public void undeploy() {

		switch (comp.getOcciComponentState().getValue()) {

		case Status.ACTIVE_VALUE:

			comp.stop();
			cmtool.undeploy(comp);
			comp.setOcciComponentState(Status.UNDEPLOYED);
			break;

		case Status.INACTIVE_VALUE:
			cmtool.undeploy(comp);
			comp.setOcciComponentState(Status.UNDEPLOYED);
			break;

		case Status.DEPLOYED_VALUE:
			cmtool.undeploy(comp);
			comp.setOcciComponentState(Status.UNDEPLOYED);
			break;

		case Status.ERROR_VALUE:
			cmtool.undeploy(comp);
			comp.setOcciComponentState(Status.UNDEPLOYED);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Publisher_Kind_deploy_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: deploy -
	 * title:
	 */
	public void deploy() {
		int status = -1;

		// Component State Machine.
		switch (comp.getOcciComponentState().getValue()) {

		case Status.UNDEPLOYED_VALUE:
			for (Component component : getInstallDependendComps()) {
				component.deploy();
			}
			status = cmtool.deploy(comp);

			if (status == 0 && assertCompsStatusEquals(getInstallDependendComps(), Status.DEPLOYED)) {
				comp.setOcciComponentState(Status.DEPLOYED);
			} else {
				comp.setOcciComponentState(Status.ERROR);
			}
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Publisher_Kind_configure_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: configure
	 * - title:
	 */
	public void configure() {
		int status = -1;

		// Component State Machine.
		switch (comp.getOcciComponentState().getValue()) {

		case Status.DEPLOYED_VALUE:
			for (Component component : getInstallDependendComps()) {
				component.configure();
			}
			status = cmtool.configure(comp);

			if (status == 0 && assertCompsStatusEquals(getInstallDependendComps(), Status.INACTIVE)) {
				comp.setOcciComponentState(Status.INACTIVE);
			} else {
				comp.setOcciComponentState(Status.ERROR);
			}
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Publisher_Kind_Start_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: start -
	 * title: Start the application.
	 */
	public void start() {
		int status = -1;

		// Component State Machine.
		switch (comp.getOcciComponentState().getValue()) {

		case Status.INACTIVE_VALUE:
			for (Component component : getExecutionDependendComps()) {
				component.start();
			}

			status = cmtool.start(comp);

			if (status == 0 && assertCompsStatusEquals(getExecutionDependendComps(), Status.ACTIVE)) {
				comp.setOcciComponentState(Status.ACTIVE);
			} else {
				comp.setOcciComponentState(Status.ERROR);
			}

			break;

		case Status.UNDEPLOYED_VALUE:
			for (Component component : getInstallDependendComps()) {
				component.deploy();
			}
			comp.deploy();

			for (Component component : getInstallDependendComps()) {
				component.configure();
			}
			comp.configure();

			for (Component component : getExecutionDependendComps()) {
				component.start();
			}

			status = cmtool.start(comp);

			if (status == 0 && assertCompsStatusEquals(getExecutionDependendComps(), Status.ACTIVE)) {
				comp.setOcciComponentState(Status.ACTIVE);
			} else {
				comp.setOcciComponentState(Status.ERROR);
			}

			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Publisher_Kind_Stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: stop -
	 * title: Stop the application.
	 */
	public void stop() {
		// Component State Machine.
		switch (comp.getOcciComponentState().getValue()) {

		case Status.ACTIVE_VALUE:

			cmtool.stop(comp);

			comp.setOcciComponentState(Status.INACTIVE);

			break;

		default:
			break;
		}
	}
	// End of user code

	private List<Component> getInstallDependendComps() {
		LinkedList<Component> dependendComponents = new LinkedList<Component>();
		for (Link link : comp.getLinks()) {
			for (MixinBase mixin : link.getParts()) {
				if (mixin instanceof Installationdependency) {
					dependendComponents.add((Component) link.getTarget());
					break;
				}
			}
		}
		return dependendComponents;
	}

	private List<Component> getExecutionDependendComps() {
		LinkedList<Component> dependendComponents = new LinkedList<Component>();
		for (Link link : comp.getLinks()) {
			for (MixinBase mixin : link.getParts()) {
				if (mixin instanceof Executiondependency) {
					dependendComponents.add((Component) link.getTarget());
					break;
				}
			}
		}
		return dependendComponents;
	}

	private boolean assertCompsStatusEquals(List<Component> components, Status status) {
		for (Component component : components) {
			if (component.getOcciComponentState().getValue() != status.getValue()) {
				return false;
			}
		}
		return true;
	}

	public static boolean isConnectedToCompute(Component comp) {
		for (Link link : comp.getLinks()) {
			if (link.getTarget() != null) {
				if (link.getTarget() instanceof Compute) {
					return true;
				}
			}
		}
		return false;
	}
}