# Sensor Creation
In this scenario the hadoop cluster model gets enhanced to monitor Memory utilization in addition to CPU utilization of worker nodes.
Therefore, a tree editor in OCCI-Studio is used as described in the following.

## Enhancing the Model for Simulation Purposes
The following description shows how to adapt the initial deployment model using an editor within OCCIWare Studio:

### Starting OCCI-Studio
1. Open the folder OCCI-Studio located on the Desktop
2. Double click on OCCI-Studio to open the IDE
3. Now you should see a checked out version of MOCCI

### Creating a new Modeling Project
1. Right click on the Project Explorer
2. Choose New->Project...
3. Search for Modeling and choose Sirius Modeling Project
4. Press Next and name the project MemMonitoring
5. Copy the de.ugoe.cs.rwm.mocci/src/main/resources/hadoopClusterCPU.occic file from the MOCCI project into the MemMonitoring project
6. Rename the file in your project to hadoopClusterCPUandMem.occic

### Utilizing the Tree Editor
1. Right click on hadoopClusterCPUandMem.occic
2. Choose Open With->OCCI Model Editor
3. Expand the first item, as well as the Configuration it contains
4. Now you can see the hadoop cluster model used for the initial deployment as shown below

![Tree](./tree.jpg "Tree")

### Adding a Memory Sensor to the Model
1. Right click on Configuration->New Child->Resource
   1. Open the properties view and set the kind of the new resource to Sensor
   2. Right click on the Sensor->New Child->Link
      1. Set the kind of the new link to MonitorableProperty
      2. Set the target of the link to the compute node it should monitor
      3. Right click on the MonitorableProperty->New Child-> Attribute State
          1. Name the attribute monitoring.property
          2. Set the value of the attribute to Mem
2. Right click on Configuration->New Child->Resource
    1. Set the kind of one resource to ResultProvider 
3. Right click on Configuration->New Child->Resource
    1. Set the kind of the other resource to DataGatherer
4. Create two links within the Resource representing the Sensor recently created: right click New Child->Link
	 1. Set the kind of both to ComponentLink
     2. Set the target of one link to the Resource representing the ResultProvider by choosing its id.
     3. Set the target of the other link to the Resource representing the DataGatherer by choosing its id.
5. Save your changes to the model. It should look similar to the model shown below:

![AdjustedTree](./adjustedTree.jpg "AdjustedTree")


### Deploying the adjusted Model
Even though the model currently does not consists of placementlinks for the new monitoring sensor which is required for an deployment in the cloud, it is sufficient to perform the simulation.
To test the enhanced model follow the enumerated instructions. Hereby, the MartServer does not have to be reseted, as the deployment engine compares the runtime model to the input model and performs only the requests required to reach the desired state.

1. Copy the hadoopClusterCPUandMem.occic file to the desktop
2. Open a terminal (Ctrl-Alt-T) and navigate to the desktop (cd Desktop)
3. Start the Mart Server (./startMart.sh)
4. Open another terminal (Ctrl-Alt-T) and navigate to the desktop (cd Desktop)
5. Execute the following command
```
java -jar initialDeployment.jar hadoopClusterCPUandMem.occic
```
This command puts your adjusted model into the models at runtime engine which is also used for the intial deployment.
To start the added sensor execute the following REST request.
The string **sensorid has to be replaced** with the id of the Resource representing the Sensor you added. E.g., *urn:uuid:c2263c17-4b46-44f7-90f7-2036ac12eb03*.
For example, curl can be used by starting another terminal (Ctrl-Alt-T).

```
curl -v -X POST http://localhost:8080/sensor/sensorid/?action=start -H 'Content-Type: text/occi'  -H 'Category: start; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
```

Finally, the added Sensors monitorable property should be filled with simulated monitoring data.
This can be checked by investigating the model through the browser or by having a look at the output of the MartServer.
```
http://localhost:8080/monitorableproperty/
```
This time two monitorableproperties should be displayed, one providing monitoring results for CPU and one for Mem.
To adjust the simulation follow the instructions given in the [dummy connector](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/tree/master/de.ugoe.cs.rwm.mocci.connector.dummy)


### Visualizing the Model
To visualize the model in an graphical Sirius  editor the following steps have to be performed:

1. Double click on representation.aird
2. Press Add... in the Models section
3. Choose Browse Workspace...
4. Select MemMonitoring->hadoopClusterCPUandMem.occic
5. Now the Model and its possible representations should been loaded (transparent)
6. Under Representations doubleclick on OCCI Configuratrion/Configuration diagram
7. Next choose Configuration diagram (0) (non-transparent)
8. Select the Configuration element and press Finish

Now a new configuration diagram is shown which has a rather large size. This is due to the large strings contained within the compute nodes.
To reduce the size of the model visualization navigate to the left and collapse the User_Data and SSH information of each compute node. Thereafter, press arrange all in the editor.
Now the cloud deployment is ready to be explored more easily.

*Note:* This editor can also be used to create and adjust OCCI models.

## Adjusting the Model for an actual deployment.
To make the model ready for an actual deployment placementlinks in each component of the sensor has to be added.
Moreover, the individual components have to be tagged with user mixins refering to their configuration management scripts.
Please refer to the [connector documentation](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/tree/master/de.ugoe.cs.rwm.mocci.connector) of the monitoring extension for more information.
The model for a concrete deployment of a memory monitoring sensor, including its configuration management script, can be found [here](../src/main/resources/hadoopClusterNewExtWithMem.occic).