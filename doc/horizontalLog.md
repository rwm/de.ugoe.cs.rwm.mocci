# Vertical Scaling Log
This file shows a short excerpt form the standard output of the horizontal scaling application.
Depending on the Monitoring results, an analyzis is performed checking whether the new worker nodes have to be added or removed from the cluster.

```
Starting MAPE loop

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:50:57 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:50:57 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:50:57 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 1| Critical CPUs: 0| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:51:09 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:51:09 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:51:09 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 1| Critical CPUs: 0| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:51:20 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:51:20 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:51:20 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 1| Critical CPUs: 1| None CPUs: 0
Analyze: Critical State Detected
Plan: upScale!
         Adding Compute Node to Model
             Ip in Hadoop Network: 10.254.1.12
         Adding Worker Component to Model
         Adding Sensor to Model
             Adding Datagatherer to Model
             Adding Dataprocessor to Model
             Adding Resultprovider to Model
Execute: Deploying adjusted Model
2019-01-26 09:51:20 INFO  Executor:329 - PUT http://localhost:8080/resultprovider/urn:uuid:b3bd44ae-2e5f-4ffe-949f-0e4124fb71a3/ -H 'Content-Type: text/occi'  -H 'Category: resultprovider; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind", occiresultprovider; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="mixin", cpupublisher; scheme="http://schemas.modmacao.org/usermixins#"; class="mixin"'  -H 'X-OCCI-Attribute:occi.core.title="CPUProvider", result.provider.endpoint="192.168.35.45:8080", occi.core.id="urn:uuid:b3bd44ae-2e5f-4ffe-949f-0e4124fb71a3"' 
2019-01-26 09:51:20 INFO  Executor:329 - PUT http://localhost:8080/dataprocessor/urn:uuid:461dbb7f-baa6-46e3-811e-a9afde4e2d09/ -H 'Content-Type: text/occi'  -H 'Category: dataprocessor; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind", cpuprocessorlocal; scheme="http://schemas.modmacao.org/usermixins#"; class="mixin"'  -H 'X-OCCI-Attribute:occi.core.title="CPUProcessor", occi.core.id="urn:uuid:461dbb7f-baa6-46e3-811e-a9afde4e2d09"' 
2019-01-26 09:51:20 INFO  Executor:329 - PUT http://localhost:8080/sensor/urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9/ -H 'Content-Type: text/occi'  -H 'Category: sensor; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.title="CPUSensor", occi.core.id="urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9"' 
2019-01-26 09:51:20 INFO  Executor:329 - PUT http://localhost:8080/datagatherer/urn:uuid:68395305-0464-45f2-be4a-fa2c9e40e89d/ -H 'Content-Type: text/occi'  -H 'Category: datagatherer; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind", cpugatherer; scheme="http://schemas.modmacao.org/usermixins#"; class="mixin"'  -H 'X-OCCI-Attribute:occi.core.title="CPUGatherer", occi.core.id="urn:uuid:68395305-0464-45f2-be4a-fa2c9e40e89d"' 
2019-01-26 09:51:20 INFO  Executor:329 - PUT http://localhost:8080/component/urn:uuid:d931f4e4-b11f-4a0e-a581-e817616baaa1/ -H 'Content-Type: text/occi'  -H 'Category: component; scheme="http://schemas.modmacao.org/occi/platform#"; class="kind", hworker; scheme="http://schemas.modmacao.org/usermixins#"; class="mixin"'  -H 'X-OCCI-Attribute:occi.core.title="worker-component", occi.core.id="urn:uuid:d931f4e4-b11f-4a0e-a581-e817616baaa1"' 
2019-01-26 09:51:20 INFO  Executor:329 - PUT http://localhost:8080/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/ -H 'Content-Type: text/occi'  -H 'Category: compute; scheme="http://schemas.ogf.org/occi/infrastructure#"; class="kind", user_data; scheme="http://schemas.ogf.org/occi/infrastructure/compute#"; class="mixin", ssh_key; scheme="http://schemas.ogf.org/occi/infrastructure/credentials#"; class="mixin", ubuntu_xenialxerus; scheme="http://schemas.modmacao.org/openstack/swe#"; class="mixin"'  -H 'X-OCCI-Attribute:occi.core.id="urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22", occi.compute.state="active", occi.core.title="hadoop-worker-additional-10-254-1-12", occi.compute.hostname="hadoop-worker-additional-10-254-1-12", occi.credentials.ssh.publickey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZG9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7ZQxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+CbNKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBuxzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova", occi.compute.userdata="I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGllIHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRlCnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBwYXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBlbnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFjZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBlbnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAogICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAogICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj"' 
2019-01-26 09:51:20 INFO  Executor:329 - PUT http://localhost:8080/componentlink/urn:uuid:89325de5-3aeb-42c6-8f68-c65af6880c7a/ -H 'Content-Type: text/occi'  -H 'Category: componentlink; scheme="http://schemas.modmacao.org/occi/platform#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/application/urn:uuid:a4888ba9-a0ea-48f2-a29e-901c876ab42d/",occi.core.target="/component/urn:uuid:d931f4e4-b11f-4a0e-a581-e817616baaa1/",occi.core.id="urn:uuid:89325de5-3aeb-42c6-8f68-c65af6880c7a"' 
2019-01-26 09:51:21 INFO  Executor:329 - PUT http://localhost:8080/componentlink/urn:uuid:79b68716-1849-49c1-8003-6476f9371e96/ -H 'Content-Type: text/occi'  -H 'Category: componentlink; scheme="http://schemas.modmacao.org/occi/platform#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/resultprovider/urn:uuid:b3bd44ae-2e5f-4ffe-949f-0e4124fb71a3/",occi.core.target="/dataprocessor/urn:uuid:461dbb7f-baa6-46e3-811e-a9afde4e2d09/",occi.core.id="urn:uuid:79b68716-1849-49c1-8003-6476f9371e96"' 
2019-01-26 09:51:21 INFO  Executor:329 - PUT http://localhost:8080/componentlink/urn:uuid:c62b2f1c-5b36-4071-be29-fbd9f1af0654/ -H 'Content-Type: text/occi'  -H 'Category: componentlink; scheme="http://schemas.modmacao.org/occi/platform#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/dataprocessor/urn:uuid:461dbb7f-baa6-46e3-811e-a9afde4e2d09/",occi.core.target="/datagatherer/urn:uuid:68395305-0464-45f2-be4a-fa2c9e40e89d/",occi.core.id="urn:uuid:c62b2f1c-5b36-4071-be29-fbd9f1af0654"' 
2019-01-26 09:51:21 INFO  Executor:329 - PUT http://localhost:8080/componentlink/urn:uuid:d55a30ae-401c-45f5-b021-ee760b04413d/ -H 'Content-Type: text/occi'  -H 'Category: componentlink; scheme="http://schemas.modmacao.org/occi/platform#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/sensor/urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9/",occi.core.target="/dataprocessor/urn:uuid:461dbb7f-baa6-46e3-811e-a9afde4e2d09/",occi.core.id="urn:uuid:d55a30ae-401c-45f5-b021-ee760b04413d"' 
2019-01-26 09:51:21 INFO  Executor:329 - PUT http://localhost:8080/componentlink/urn:uuid:7a9b3046-ed5f-41ae-a359-5bbdf15c851b/ -H 'Content-Type: text/occi'  -H 'Category: componentlink; scheme="http://schemas.modmacao.org/occi/platform#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/sensor/urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9/",occi.core.target="/datagatherer/urn:uuid:68395305-0464-45f2-be4a-fa2c9e40e89d/",occi.core.id="urn:uuid:7a9b3046-ed5f-41ae-a359-5bbdf15c851b"' 
2019-01-26 09:51:21 INFO  Executor:329 - PUT http://localhost:8080/componentlink/urn:uuid:c8ce4ff8-7e25-4213-90eb-9eac4d64ddb7/ -H 'Content-Type: text/occi'  -H 'Category: componentlink; scheme="http://schemas.modmacao.org/occi/platform#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/sensor/urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9/",occi.core.target="/resultprovider/urn:uuid:b3bd44ae-2e5f-4ffe-949f-0e4124fb71a3/",occi.core.id="urn:uuid:c8ce4ff8-7e25-4213-90eb-9eac4d64ddb7"' 
2019-01-26 09:51:30 INFO  Executor:154 - GET http://localhost:8080/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/
2019-01-26 09:51:31 INFO  Executor:329 - PUT http://localhost:8080/networkinterface/urn:uuid:dab3243f-f1c0-42a2-b9c9-a0211b1867ec/ -H 'Content-Type: text/occi'  -H 'Category: networkinterface; scheme="http://schemas.ogf.org/occi/infrastructure#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/",occi.core.target="/network/urn:uuid:7a9fca2c-24fb-473c-aa9c-8dc9e68a432a/",occi.core.id="urn:uuid:dab3243f-f1c0-42a2-b9c9-a0211b1867ec"' 
2019-01-26 09:51:31 INFO  Executor:329 - PUT http://localhost:8080/placementlink/urn:uuid:dd16dfc1-ecf6-4389-a2bb-0c9987309d3d/ -H 'Content-Type: text/occi'  -H 'Category: placementlink; scheme="http://schemas.modmacao.org/placement#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/component/urn:uuid:d931f4e4-b11f-4a0e-a581-e817616baaa1/",occi.core.target="/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/",occi.core.id="urn:uuid:dd16dfc1-ecf6-4389-a2bb-0c9987309d3d"' 
2019-01-26 09:51:31 INFO  Executor:329 - PUT http://localhost:8080/placementlink/urn:uuid:7b330c3d-b85b-4cb6-b6e5-023fbd8bdd81/ -H 'Content-Type: text/occi'  -H 'Category: placementlink; scheme="http://schemas.modmacao.org/placement#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/dataprocessor/urn:uuid:461dbb7f-baa6-46e3-811e-a9afde4e2d09/",occi.core.target="/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/",occi.core.id="urn:uuid:7b330c3d-b85b-4cb6-b6e5-023fbd8bdd81"' 
2019-01-26 09:51:31 INFO  Executor:329 - PUT http://localhost:8080/placementlink/urn:uuid:6591447b-5e7b-4596-94a8-59e421cf5957/ -H 'Content-Type: text/occi'  -H 'Category: placementlink; scheme="http://schemas.modmacao.org/placement#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/resultprovider/urn:uuid:b3bd44ae-2e5f-4ffe-949f-0e4124fb71a3/",occi.core.target="/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/",occi.core.id="urn:uuid:6591447b-5e7b-4596-94a8-59e421cf5957"' 
2019-01-26 09:51:31 INFO  Executor:329 - PUT http://localhost:8080/networkinterface/urn:uuid:d61cdc1c-bc82-407f-9db9-fffbf2fa0d79/ -H 'Content-Type: text/occi'  -H 'Category: networkinterface; scheme="http://schemas.ogf.org/occi/infrastructure#"; class="kind", ipnetworkinterface; scheme="http://schemas.ogf.org/occi/infrastructure/networkinterface#"; class="mixin"'  -H 'X-OCCI-Attribute:occi.core.source="/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/",occi.core.target="/network/urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339591/",occi.core.id="urn:uuid:d61cdc1c-bc82-407f-9db9-fffbf2fa0d79", occi.networkinterface.address="10.254.1.12"' 
2019-01-26 09:51:31 INFO  Executor:329 - PUT http://localhost:8080/monitorableproperty/urn:uuid:e3dc8254-bd48-42c8-a824-0d86075aba67/ -H 'Content-Type: text/occi'  -H 'Category: monitorableproperty; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/sensor/urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9/",occi.core.target="/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/",occi.core.id="urn:uuid:e3dc8254-bd48-42c8-a824-0d86075aba67", occi.core.title="monProp", monitoring.property="CPU"' 
2019-01-26 09:51:31 INFO  Executor:329 - PUT http://localhost:8080/placementlink/urn:uuid:9ad58554-214e-49a8-8334-8cbdcf2bb6ab/ -H 'Content-Type: text/occi'  -H 'Category: placementlink; scheme="http://schemas.modmacao.org/placement#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="/datagatherer/urn:uuid:68395305-0464-45f2-be4a-fa2c9e40e89d/",occi.core.target="/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/",occi.core.id="urn:uuid:9ad58554-214e-49a8-8334-8cbdcf2bb6ab"' 
2019-01-26 09:51:31 INFO  Executor:329 - PUT http://localhost:8080/networkinterface/urn:uuid:d936921f-d806-4a13-8938-6676df0b342f/ -H 'Content-Type: text/occi'  -H 'Category: networkinterface; scheme="http://schemas.ogf.org/occi/infrastructure#"; class="kind", ipnetworkinterface; scheme="http://schemas.ogf.org/occi/infrastructure/networkinterface#"; class="mixin", ansibleendpoint; scheme="http://schemas.modmacao.org/occi/ansible#"; class="mixin"'  -H 'X-OCCI-Attribute:occi.core.source="/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/",occi.core.target="/network/urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590/",occi.core.id="urn:uuid:d936921f-d806-4a13-8938-6676df0b342f", occi.core.title="hadoop-worker-additional-10-254-1-12 -> Management Network"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/application/urn:uuid:a4888ba9-a0ea-48f2-a29e-901c876ab42d/?action=deploy -H 'Content-Type: text/occi'  -H 'Category: deploy; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/application/urn:uuid:a4888ba9-a0ea-48f2-a29e-901c876ab42d/?action=configure -H 'Content-Type: text/occi'  -H 'Category: configure; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/application/urn:uuid:a4888ba9-a0ea-48f2-a29e-901c876ab42d/?action=start -H 'Content-Type: text/occi'  -H 'Category: start; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/sensor/urn:uuid:efb0f50a-7a7c-4153-b939-4846d6554dbb/?action=deploy -H 'Content-Type: text/occi'  -H 'Category: deploy; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/sensor/urn:uuid:efb0f50a-7a7c-4153-b939-4846d6554dbb/?action=configure -H 'Content-Type: text/occi'  -H 'Category: configure; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/sensor/urn:uuid:efb0f50a-7a7c-4153-b939-4846d6554dbb/?action=start -H 'Content-Type: text/occi'  -H 'Category: start; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/sensor/urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9/?action=deploy -H 'Content-Type: text/occi'  -H 'Category: deploy; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/sensor/urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9/?action=configure -H 'Content-Type: text/occi'  -H 'Category: configure; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/sensor/urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9/?action=start -H 'Content-Type: text/occi'  -H 'Category: start; scheme="http://schemas.modmacao.org/occi/platform/application/action#"; class="action"' 
2019-01-26 09:51:31 INFO  Executor:105 - POST http://localhost:8080/component/urn:uuid:d931f4e4-b11f-4a0e-a581-e817616baaa1/?action=start -H 'Content-Type: text/occi'  -H 'Category: start; scheme="http://schemas.modmacao.org/occi/platform/component/action#"; class="action"' 

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:51:41 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:51:41 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:51:41 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 1| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:51:51 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:51:51 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:51:51 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 0| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:52:02 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:52:02 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:52:02 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 0| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:52:12 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:52:12 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:52:12 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 0| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:52:23 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:52:23 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:52:23 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 0| None CPUs: 1
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:52:33 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:52:33 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:52:33 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 0| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:52:44 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:52:44 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:52:44 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 0| None CPUs: 1
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:52:54 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:52:54 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:52:54 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 1| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:53:04 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:53:04 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:53:04 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 0| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:53:15 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:53:15 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:53:15 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 2| Critical CPUs: 0| None CPUs: 1
Analyze: Non Critical State Detected
Plan: downScale!
      VM with None CPU utilization found: urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22
      Deleting Entities Around: hadoop-worker-additional-10-254-1-12 (urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22)
Execute: Deploying adjusted Model
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/componentlink/urn:uuid:89325de5-3aeb-42c6-8f68-c65af6880c7a/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/componentlink/urn:uuid:c62b2f1c-5b36-4071-be29-fbd9f1af0654/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/componentlink/urn:uuid:7a9b3046-ed5f-41ae-a359-5bbdf15c851b/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/componentlink/urn:uuid:c8ce4ff8-7e25-4213-90eb-9eac4d64ddb7/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/componentlink/urn:uuid:d55a30ae-401c-45f5-b021-ee760b04413d/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/monitorableproperty/urn:uuid:e3dc8254-bd48-42c8-a824-0d86075aba67/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/componentlink/urn:uuid:79b68716-1849-49c1-8003-6476f9371e96/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/networkinterface/urn:uuid:d61cdc1c-bc82-407f-9db9-fffbf2fa0d79/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/networkinterface/urn:uuid:dab3243f-f1c0-42a2-b9c9-a0211b1867ec/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/networkinterface/urn:uuid:d936921f-d806-4a13-8938-6676df0b342f/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/compute/urn:uuid:22d0da9c-d31f-455a-bca0-44db27dbad22/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/dataprocessor/urn:uuid:461dbb7f-baa6-46e3-811e-a9afde4e2d09/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/sensor/urn:uuid:491299ac-2940-4d00-a995-ef0c7a5de6b9/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/resultprovider/urn:uuid:b3bd44ae-2e5f-4ffe-949f-0e4124fb71a3/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/component/urn:uuid:d931f4e4-b11f-4a0e-a581-e817616baaa1/
2019-01-26 09:53:15 INFO  Executor:251 - DELETE http://localhost:8080/datagatherer/urn:uuid:68395305-0464-45f2-be4a-fa2c9e40e89d/

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
2019-01-26 09:53:25 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical
2019-01-26 09:53:25 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=None
2019-01-26 09:53:25 INFO  Executor:378 - GET http://localhost:8080/monitorableproperty?attribute=monitoring.property&value=CPU
Monitor: Monitored CPUs: 1| Critical CPUs: 0| None CPUs: 0
Analyze: Non Critical State Detected
Plan: downScale!
      Every Compute busy/Only one worker! Skipping downScale!
Execute: Deploying adjusted Model

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------

```