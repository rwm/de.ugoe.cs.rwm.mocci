# Vertical Scaling Log
This file shows a short excerpt form the standard output of the vertical.sh script.
Depending on the Monitoring results, an analyzis is performed checking whether the VM has to up- or downscaled followed by an PUT request updating the amount of cores of the VM.

```
Starting MAPE script
Requesting http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical every 3 seconds!
Monitor
{
  "id" : "urn:uuid:ba16f4ee-1601-4192-a259-eae4274aed72",
  "kind" : "http://schemas.ugoe.cs.rwm/monitoring#monitorableproperty",
  "mixins" : [ ],
  "attributes" : {
    "monitoring.property" : "CPU",
    "monitoring.result" : "Critical"
  },
  "actions" : [ ],
  "location" : "/monitorableproperty/urn:uuid:ba16f4ee-1601-4192-a259-eae4274aed72",
  "source" : {
    "location" : "/sensor/urn:uuid:efb0f50a-7a7c-4153-b939-4846d6554dbb",
    "kind" : "http://schemas.ugoe.cs.rwm/monitoring#sensor"
  },
  "target" : {
    "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
    "kind" : "http://schemas.ogf.org/occi/infrastructure#compute"
  }
}
Analyze
Critical Compute Detected
Plan: Scale up VM
State: UpScaled
Execute
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> PUT /compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf/ HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.47.0
> Accept: */*
> Content-Type: text/occi
> Category: compute; scheme="http://schemas.ogf.org/occi/infrastructure#"; class="kind", ssh_key; scheme="http://schemas.ogf.org/occi/infrastructure/credentials#"; class="mixin", user_data; scheme="http://schemas.ogf.org/occi/infrastructure/compute#"; class="mixin", ubuntu_xenialxerus; scheme="http://schemas.modmacao.org/openstack/swe#"; class="mixin"
> X-OCCI-Attribute:occi.core.id="urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf", occi.core.title="hadoop-worker-1", occi.core.summary="", occi.compute.architecture="x86", occi.compute.cores="8", occi.compute.hostname="hadoop-worker-1", occi.compute.share="0", occi.compute.speed="0", occi.compute.memory="8192", occi.compute.state="active", occi.compute.state.message="", occi.credentials.ssh.publickey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZG9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7ZQxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+CbNKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBuxzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova", occi.compute.userdata="I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGllIHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRlCnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBwYXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBlbnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFjZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBlbnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAogICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAogICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj"
> 
< HTTP/1.1 201 Created
< Date: Tue, 29 Jan 2019 15:11:21 GMT
< Server: OCCIWare MART Server v1.0 OCCI/1.2
< Accept: text/occi;application/json;application/occi+json;text/plain;text/occi+plain
< Content-Type: application/json
< Location: http://localhost:8080/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf
< Content-Length: 6337
< 
{
  "id" : "urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
  "kind" : "http://schemas.ogf.org/occi/infrastructure#compute",
  "mixins" : [ "http://schemas.ogf.org/occi/infrastructure/credentials#ssh_key", "http://schemas.ogf.org/occi/infrastructure/compute#user_data", "http://schemas.modmacao.org/openstack/swe#ubuntu_xenialxerus" ],
  "attributes" : {
    "occi.compute.architecture" : "x86",
    "occi.compute.cores" : 8,
    "occi.compute.hostname" : "hadoop-worker-1",
    "occi.compute.share" : 0,
    "occi.compute.speed" : 0.0,
    "occi.compute.memory" : 8192.0,
    "occi.compute.state" : "active",
    "occi.compute.state.message" : "",
    "occi.credentials.ssh.publickey" : "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZG9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7ZQxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+CbNKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBuxzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova",
    "occi.compute.userdata" : "I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGllIHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRlCnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBwYXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBlbnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFjZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBlbnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAogICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAogICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj",
    "openstack.runtime.id" : "5437eae8-7c47-4834-929c-a314de77d291"
  },
  "links" : [ {
    "id" : "urn:uuid:356f7b59-69a7-4df1-9ab5-c0a46b49b9d1",
    "kind" : "http://schemas.ogf.org/occi/infrastructure#networkinterface",
    "mixins" : [ "http://schemas.modmacao.org/occi/ansible#ansibleendpoint", "http://schemas.ogf.org/occi/infrastructure/networkinterface#ipnetworkinterface" ],
    "attributes" : {
      "occi.networkinterface.interface" : "",
      "occi.networkinterface.mac" : "",
      "occi.networkinterface.state" : "active",
      "occi.networkinterface.state.message" : "",
      "occi.networkinterface.address" : "",
      "occi.networkinterface.gateway" : "",
      "occi.networkinterface.allocation" : "dynamic"
    },
    "actions" : [ ],
    "location" : "/networkinterface/urn:uuid:356f7b59-69a7-4df1-9ab5-c0a46b49b9d1",
    "source" : {
      "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#compute"
    },
    "target" : {
      "location" : "/network/urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#network"
    }
  }, {
    "id" : "urn:uuid:03f91178-136f-4023-876e-84509f8a5a2d",
    "kind" : "http://schemas.ogf.org/occi/infrastructure#networkinterface",
    "mixins" : [ "http://schemas.ogf.org/occi/infrastructure/networkinterface#ipnetworkinterface" ],
    "attributes" : {
      "occi.networkinterface.interface" : "",
      "occi.networkinterface.mac" : "",
      "occi.networkinterface.state" : "active",
      "occi.networkinterface.state.message" : "",
      "occi.networkinterface.address" : "100.254.1.35",
      "occi.networkinterface.gateway" : "",
      "occi.networkinterface.allocation" : "dynamic"
    },
    "actions" : [ ],
    "location" : "/networkinterface/urn:uuid:03f91178-136f-4023-876e-84509f8a5a2d",
    "source" : {
      "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#compute"
    },
    "target" : {
      "location" : "/network/urn:uuid:7a9fca2c-24fb-473c-aa9c-8dc9e68a432a",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#network"
    }
  }, {
    "id" : "urn:uuid:c8c49905-3d5e-43b2-8d09-fabf92d29722",
    "kind" : "http://schemas.ogf.org/occi/infrastructure#networkinterface",
    "mixins" : [ "http://schemas.ogf.org/occi/infrastructure/networkinterface#ipnetworkinterface" ],
    "attributes" : {
      "occi.networkinterface.interface" : "",
      "occi.networkinterface.mac" : "",
      "occi.networkinterface.state" : "active",
      "occi.networkinterface.state.message" : "",
      "occi.networkinterface.address" : "10.254.1.8",
      "occi.networkinterface.gateway" : "",
      "occi.networkinterface.allocation" : "dynamic"
    },
    "actions" : [ ],
    "location" : "/networkinterface/urn:uuid:c8c49905-3d5e-43b2-8d09-fabf92d29722",
    "source" : {
      "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#compute"
    },
    "target" : {
      "location" : "/network/urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339591",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#network"
    }
  } ],
  "actions" : [ "http://schemas.ogf.org/occi/infrastructure/compute/action#start", "http://schemas.ogf.org/occi/infrastructure/compute/action#stop", "http://schemas.ogf.org/occi/infrastructure/compute/action#restart", "http://schemas.ogf.org/occi/infrastructure/compute/action#suspend", "http://schemas.ogf.org/occi/infrastructure/compute/action#save" ],
  "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf"
}
* Connection #0 to host localhost left intact
Monitor
{
  "id" : "urn:uuid:ba16f4ee-1601-4192-a259-eae4274aed72",
  "kind" : "http://schemas.ugoe.cs.rwm/monitoring#monitorableproperty",
  "mixins" : [ ],
  "attributes" : {
    "monitoring.property" : "CPU",
    "monitoring.result" : "Critical"
  },
  "actions" : [ ],
  "location" : "/monitorableproperty/urn:uuid:ba16f4ee-1601-4192-a259-eae4274aed72",
  "source" : {
    "location" : "/sensor/urn:uuid:efb0f50a-7a7c-4153-b939-4846d6554dbb",
    "kind" : "http://schemas.ugoe.cs.rwm/monitoring#sensor"
  },
  "target" : {
    "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
    "kind" : "http://schemas.ogf.org/occi/infrastructure#compute"
  }
}
Analyze
Critical Compute Detected
Plan: Scale up VM
Monitor
{ }
Analyze
No Crtical Compute Detected
Plan: Scale down VM
State: DownScaled
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> PUT /compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf/ HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.47.0
> Accept: */*
> Content-Type: text/occi
> Category: compute; scheme="http://schemas.ogf.org/occi/infrastructure#"; class="kind", ssh_key; scheme="http://schemas.ogf.org/occi/infrastructure/credentials#"; class="mixin", user_data; scheme="http://schemas.ogf.org/occi/infrastructure/compute#"; class="mixin", ubuntu_xenialxerus; scheme="http://schemas.modmacao.org/openstack/swe#"; class="mixin"
> X-OCCI-Attribute:occi.core.id="urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf", occi.core.title="hadoop-worker-1", occi.core.summary="", occi.compute.architecture="x86", occi.compute.cores="2", occi.compute.hostname="hadoop-worker-1", occi.compute.share="0", occi.compute.speed="0", occi.compute.memory="4096", occi.compute.state="active", occi.compute.state.message="", occi.credentials.ssh.publickey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZG9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7ZQxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+CbNKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBuxzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova", occi.compute.userdata="I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGllIHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRlCnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBwYXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBlbnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFjZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBlbnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAogICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAogICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj"
> 
< HTTP/1.1 201 Created
< Date: Tue, 29 Jan 2019 15:11:27 GMT
< Server: OCCIWare MART Server v1.0 OCCI/1.2
< Accept: text/occi;application/json;application/occi+json;text/plain;text/occi+plain
< Content-Type: application/json
< Location: http://localhost:8080/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf
< Content-Length: 6337
< 
{
  "id" : "urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
  "kind" : "http://schemas.ogf.org/occi/infrastructure#compute",
  "mixins" : [ "http://schemas.ogf.org/occi/infrastructure/credentials#ssh_key", "http://schemas.ogf.org/occi/infrastructure/compute#user_data", "http://schemas.modmacao.org/openstack/swe#ubuntu_xenialxerus" ],
  "attributes" : {
    "occi.compute.architecture" : "x86",
    "occi.compute.cores" : 2,
    "occi.compute.hostname" : "hadoop-worker-1",
    "occi.compute.share" : 0,
    "occi.compute.speed" : 0.0,
    "occi.compute.memory" : 4096.0,
    "occi.compute.state" : "active",
    "occi.compute.state.message" : "",
    "occi.credentials.ssh.publickey" : "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZG9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7ZQxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+CbNKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBuxzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova",
    "occi.compute.userdata" : "I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGllIHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRlCnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBwYXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBlbnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFjZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBlbnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAogICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAogICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj",
    "openstack.runtime.id" : "5437eae8-7c47-4834-929c-a314de77d291"
  },
  "links" : [ {
    "id" : "urn:uuid:356f7b59-69a7-4df1-9ab5-c0a46b49b9d1",
    "kind" : "http://schemas.ogf.org/occi/infrastructure#networkinterface",
    "mixins" : [ "http://schemas.modmacao.org/occi/ansible#ansibleendpoint", "http://schemas.ogf.org/occi/infrastructure/networkinterface#ipnetworkinterface" ],
    "attributes" : {
      "occi.networkinterface.interface" : "",
      "occi.networkinterface.mac" : "",
      "occi.networkinterface.state" : "active",
      "occi.networkinterface.state.message" : "",
      "occi.networkinterface.address" : "",
      "occi.networkinterface.gateway" : "",
      "occi.networkinterface.allocation" : "dynamic"
    },
    "actions" : [ ],
    "location" : "/networkinterface/urn:uuid:356f7b59-69a7-4df1-9ab5-c0a46b49b9d1",
    "source" : {
      "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#compute"
    },
    "target" : {
      "location" : "/network/urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#network"
    }
  }, {
    "id" : "urn:uuid:03f91178-136f-4023-876e-84509f8a5a2d",
    "kind" : "http://schemas.ogf.org/occi/infrastructure#networkinterface",
    "mixins" : [ "http://schemas.ogf.org/occi/infrastructure/networkinterface#ipnetworkinterface" ],
    "attributes" : {
      "occi.networkinterface.interface" : "",
      "occi.networkinterface.mac" : "",
      "occi.networkinterface.state" : "active",
      "occi.networkinterface.state.message" : "",
      "occi.networkinterface.address" : "100.254.1.35",
      "occi.networkinterface.gateway" : "",
      "occi.networkinterface.allocation" : "dynamic"
    },
    "actions" : [ ],
    "location" : "/networkinterface/urn:uuid:03f91178-136f-4023-876e-84509f8a5a2d",
    "source" : {
      "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#compute"
    },
    "target" : {
      "location" : "/network/urn:uuid:7a9fca2c-24fb-473c-aa9c-8dc9e68a432a",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#network"
    }
  }, {
    "id" : "urn:uuid:c8c49905-3d5e-43b2-8d09-fabf92d29722",
    "kind" : "http://schemas.ogf.org/occi/infrastructure#networkinterface",
    "mixins" : [ "http://schemas.ogf.org/occi/infrastructure/networkinterface#ipnetworkinterface" ],
    "attributes" : {
      "occi.networkinterface.interface" : "",
      "occi.networkinterface.mac" : "",
      "occi.networkinterface.state" : "active",
      "occi.networkinterface.state.message" : "",
      "occi.networkinterface.address" : "10.254.1.8",
      "occi.networkinterface.gateway" : "",
      "occi.networkinterface.allocation" : "dynamic"
    },
    "actions" : [ ],
    "location" : "/networkinterface/urn:uuid:c8c49905-3d5e-43b2-8d09-fabf92d29722",
    "source" : {
      "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#compute"
    },
    "target" : {
      "location" : "/network/urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339591",
      "kind" : "http://schemas.ogf.org/occi/infrastructure#network"
    }
  } ],
  "actions" : [ "http://schemas.ogf.org/occi/infrastructure/compute/action#start", "http://schemas.ogf.org/occi/infrastructure/compute/action#stop", "http://schemas.ogf.org/occi/infrastructure/compute/action#restart", "http://schemas.ogf.org/occi/infrastructure/compute/action#suspend", "http://schemas.ogf.org/occi/infrastructure/compute/action#save" ],
  "location" : "/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf"
}
* Connection #0 to host localhost left intact
Monitor
{ }
Analyze
No Crtical Compute Detected
Plan: Scale down VM


```