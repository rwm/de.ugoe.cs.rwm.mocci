# Setting up the MartServer to be used in OpenStack
To manage a cloud deployment using the MartServer it has to be deployed on Virtual Machine within the cloud that has to be connected to a management network.


## Configuring the MartServer in the Cloud
First you need to setup a Virtual Machine hosting the MartServer:
1. Start a Virtual Machine (we used an Ubuntu 16.04 image) 
2. Insert the OpenStack Controller IP in /etc/hosts. For example: 192.168.34.1 controller.
3. Install [ansible](https://docs.ansible.com/) on the machine running the MART Server.
4. Deploy either the Docker container or export the MartServer project.

Moreover, a management network is required over which the MartServer connects to spawned Virtual Machines:
1. Create a network
2. Attach the MartServer VM to the network

A figure depicting the general setup is shown below:

<img src="./Martoverview.jpg" width="500">

## Extension adjustments
When performing the proposed approach in a cloud, some connectors have to be configured to gain access to specific information, e.g.,  user credentials.
Moreover, each cloud provider has different entry points for their API and may have different kinds of offers regarding their offered vm images and flavors.

### Configuration Tool Adjustments
To use the configuration management tools of MoDMaCAO some adjustments have to be performed.
These adjustments configure the path to the ansible roles located on the MART server, as well as the location of the playbook.
Moreover, the ansible user and the location of the private key to be used have to be configured.

The settings itself can be found at martserver-plugins/org.modmacao.cm.jar in the ansible.properties file.
An example of this configuraiton is shown in the following Listing:
```
ansible_rolespath = /home/ubuntu/roles
ansible_bin = /usr/bin/ansible-playbook
ansible_user = ubuntu
private_key_path = /home/ubuntu/.ssh/key.pem
```

### Provider specifics
In order to translate the REST requests to requests of the cloud API, the MART server requires a connector for the corresponding cloud. 
Therefore, the connector's jar has to be placed in the martserver-plugins folder before the server is started. 
Currently, only a prototypical [connector for OpenStack](https://github.com/occiware/MoDMaCAO) is provided as part of the MoDMaCAO framework.
However, also this connector has to be configured.
The settings itself can be found at in martserver-plugins/org.modmacao.openstack.connector.jar in the openstack.properties file. An example is shown in the following Listing:
```
openstack_username = username
openstack_tenant = testTenant
openstack_password = password
openstack_endpoint = http://192.168.34.1:5000/v2.0
openstack_default_network = d52754e0-6729-4034-adbb-8f1f3800f2c6
openstack_default_image = adf63ddc-debe-4d7e-b899-b936e989439f
openstack_default_flavor = 36637a26-fded-4635-b6c5-ec8ec0745eab
```
*Note:* This connector currently only supports version 2 of the openstack authentication service.
*Note:* In our scenarios we have chosen the public network as default network to let spawned VMs download software packages defined within the configuration management scripts.

### Images and Flavors
Each vm possesses a flavor and an image. While the image describes the OS of the compute instance, the flavor describes its size.
Both attributes are depended on the used cloud provider, as they e.g., may offer different kinds of OS. To adjust your model according to this, different offers two approaches can be chosen:

#### Set default values in the connector
As already shown, a standard image and flavor can be configured within the OpenStack Connector.
This means, if no templates are attached to the compute instance regarding its size and OS the default values are used.
Thus, this configuration is rather convenient but limits the used to one kind of image and flavor for all VM used.

#### Create a new OCCI Extension fitting to the provider
If these settings are not enough, an OCCI Extension representing the offer of the cloud provider has to be created.
An example of such an extension is given in martserver-plugins\org.modmacao.openstack.swe.runtime.jar. 
In order to create your own extension, we recommend using OCCI-Studio, as it provides not only a Sirius designer for the Extension, but also allows to completely generate the required code from the modeled Extension.
Then, the Extension has only be packed and put into the martserver-plugins folder to be ready for use. 
 
*Note:* When creating a new Extension it has to be registered within the IDE, model, and code.

### User Data Adjustments
User data describes a specific behavior which shall be executed when starting a VM for the first time.
n the OCCI model, the user data is described as a Mixin within each compute resource encoded as Base64.
When encoded the Base64 represents a cloud init script. 
This has to be configured in such a manner that hot-plugging of networkinterfaces is allowed in each VM.
These may differ according to the network managing tool of the VM.
Furthermore,  Python 2.7 has to be installed on the used VM.
If not already part of the image it can also be installed via cloud init at startup.
Therefore, the following script can be used:
```
#cloud-config
package_upgrade: true
packages:
    - python 
```