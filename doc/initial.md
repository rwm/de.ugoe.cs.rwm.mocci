# Initial Deployment Tutorial
In this tutorial a hadoop cluster with one worker node getting monitored is deployed. Therefore, the [MartServer](https://github.com/occiware/MartServer) is used in combination with [DOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci) an engine automatically deploying OCCI models.
This deployment serves as entry point for the other provided scenarios: [Vertical Scaling](./vertical.md), [Horizontal Scaling](./horizontal.md), and [Sensor Creation](./own.md) showing how the monitored information can be used in self-adaptive control loops.

## Starting the MartServer
If the getting started VM is used everything is preconfigured only requiring to start the following script.
Start a terminal (Ctrl-Alt-T) and navigate to the desktop (cd Desktop). The script can be started using the following command:
```
./startMART.sh
```
If the preconfigured VM is not used: Start the MartServer with these [plugins](../src/test/resources/martserver-plugins) for the test environemt.
For an actual deployment in an OpenStack cloud we used the following [live connectors](../src/test/resources/martserver-plugins/live) including these [ansible roles](../src/test/resources/roles). 

## Deploying the Hadoop Cluster
Now that the MartServer is started the hadoop cluster can be deployed. Therefore, start the InitialDeployment.java file as an Java Application. This application performs requests to deploy the model shown below.
If the VM is used: Open a terminal (Ctrl-Alt-T) and navigate to the VM's desktop (cd Desktop) and execute the initialDeployment.jar using the following command. 
```
java -jar initialDeployment.jar
```

### Initial Deployment Application - Output
During execution of the initialDeployment.jar the Executor of the deployment engine is logged. This log shows all OCCI requests that are executed to deploy the hadoop cluster model shown beneath.
For example the network connecting the nodes in the hadoop cluster is created as shown in the following log output. The full log can be found [here](./initialLog.md):
```
2019-01-29 14:56:47 INFO  Executor:329 - PUT http://localhost:8080/network/urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339591/ -H 'Content-Type: text/occi'  -H 'Category: network; scheme="http://schemas.ogf.org/occi/infrastructure#"; class="kind", ipnetwork; scheme="http://schemas.ogf.org/occi/infrastructure/network#"; class="mixin"'  -H 'X-OCCI-Attribute:occi.core.id="urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339591", occi.core.title="HNetwork", occi.core.summary="", occi.network.vlan="0", occi.network.label="", occi.network.state="active", occi.network.state.message="", occi.network.address="10.254.1.1/24"'
```

### MartServer - Output
After the deployment has been performed. The sensors are started updating attached monitoring properties according to the interval specified in the dummy connector. The console running the MartServer puts out the simulated monitoring data in the following form:
```
INFO MonProp: CPU, set to: High(ba16f4ee-1601-4192-a259-eae4274aed72)
INFO MonProp: CPU, set to: Low(ba16f4ee-1601-4192-a259-eae4274aed72)
INFO MonProp: CPU, set to: Critical(ba16f4ee-1601-4192-a259-eae4274aed72)
```


### Browser - Output
Now that everything has been deployed you can investigate the OCCI runtime model by opening your browser and query for OCCI entitites. In the following you find some example queries for the runtime model to query all compute, sensor, and monitorableproperties, as well as a query for the concrete monitored hadoop-worker compute node, and a filter for each monitorableproperty set to Critical. Additional query and filters can be found in the [documentation of the MartServer](https://github.com/occiware/MartServer/blob/master/doc/userdoc.md). It should be noted, that the updated MonitorableProperties can be investigated by **refreshing the browser**.
```
http://localhost:8080/compute
http://localhost:8080/sensor
http://localhost:8080/monitorableproperty
http://localhost:8080/compute/urn:uuid:2e6a73d0-faaa-476a-bd25-ca461dd166cf
http://localhost:8080/monitorableproperty?attribute=monitoring.result&value=Critical

```

Now that the deployed model shown below is running, the other tutorials can be executed. The following figure visualized the initially deployed model:
![Init](./src/main/resources/de/ugoe/cs/rwm/mocci/occi/initial.jpg "Init")

## Resetting the MartServer
If you plan to execute the other scenarios ([Vertical Scaling](./vertical.md), [Horizontal Scaling](./horizontal.md), and [Sensor Creation](./own.md)) do not reset the MartServer.
Otherwise first stop the MartServer it by pressing Ctrl-C in the terminal running it. Thereafter, either start the resetMart.sh script or go to the folder holding the occi model of the MartServer(~/models) and delete the file model-anonymous.occic. This file represents the concrete runtime model stored by the MartServer when he is stopped and started.